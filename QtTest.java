import java.awt.Point;

public class QtTest
{
    public static void main(String[] args)
    {
        QuadTree qt = new QuadTree(1024);
        Point[] points = new Point[500];

        for (int i = 0; i < points.length; ++i) {
            points[i] = new Point((int)(Math.random()*1024),(int)(Math.random()*1024));
            qt.Insert(points[i]);
        }

        for (int i = 0; i < points.length; ++i) {
            qt.Remove(points[i]);
        }
    }
}
