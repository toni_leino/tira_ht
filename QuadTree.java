import java.awt.Point;
import java.io.PrintStream;

/**
 * A QuadTree node.
 */
final class Node
{
    /**
     * Constructor.
     *
     * @param x1 upper left corner x-coordinate
     * @param y1 upper left corner y-coordinate
     * @param x2 lower right corner x-coordinate
     * @param y2 lower right corner y-coordinate
     */
    public Node(int x1, int y1, int x2, int y2)
    {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        point = null;
        children = null;
    }

    /**
     * Inserts a new point in the tree.
     *
     * @param p point to insert
     */
    public void insert(Point p)
    {
        assert point == null || children == null : "Invalid Quadtree node!";
        
        if (point == null && children == null) {
            point = new Point(p);
        } else {
            if (children == null) {
                createChildNodes();
                // remove the existing point and store it in one of the children
                int quadrant = getQuadrant(point);
                children[quadrant].insert(point);
                point = null;
            }
            // insert the new point too
            children[getQuadrant(p)].insert(p);
        }
    }

    /**
     * Removes a point from the tree.
     *
     * @param p point to remove
     * @return true if successful, false if point not found
     */
    public boolean remove(Point p)
    {
        assert point == null || children == null;
        if (children != null) {
            int quadrant = getQuadrant(p);
            if (quadrant == -1) {
                // something's really wrong
                return false;
            }
            boolean success = children[quadrant].remove(p);
            if (success) {
                pruneLeaves();
            }
            return success;
        } else if (point != null) {
            if (point.equals(p)) {
                // found the point
                point = null;
                return true;
            } else {
                // point not found in the tree
                return false;
            }
        }
        return false;
    }

    /**
     * Finds a point in the tree.
     *
     * @param p point to search for
     * @return true if point found in the tree
     */
    public boolean find(Point p)
    {
        assert point == null || children == null;
        if (children != null) {
            int quadrant = getQuadrant(p);
            if (quadrant == -1) {
                return false;
            }
            return children[quadrant].find(p);
        } else if (point != null) {
            return point.equals(p);
        } else {
            return false;
        }
    }

    /**
     * Prints all points in preorder.
     *
     * @param stream stream to print the points to
     */
    public void printPoints(PrintStream stream)
    {
        if (point != null) {
            stream.println(point.x + ", " + point.y);
        }

        // recursively print children's points
        if (children != null) {
            for (int i = 0; i < children.length; ++i) {
                children[i].printPoints(stream);
            }
        }
    }

    /**
     * Checks which quadrant the given point is in.
     *
     * @param p the point to check
     * @return quadrant number (0-3) or -1 on error.
     */
    private int getQuadrant(Point p)
    {
        final int x = p.x;
        final int y = p.y;

        // check out of bounds
        if (x < x1 || x > x2 || y < y1 || y > y2) {
            return -1;
        }

        final float horizMiddle = (x1 + x2) / 2.0f;
        final float vertMiddle  = (y1 + y2) / 2.0f;

        if (y < vertMiddle) {
            if (x > horizMiddle) {
                return 0; // top right
            } else {
                return 1; // top left
            }
        } else {
            if (x < horizMiddle) {
                return 2; // bottom left
            } else {
                return 3; // bottom right
            }
        }
    }

    /**
     * Creates child nodes.
     */
    private void createChildNodes()
    {
        assert children == null;

        final float horizMiddle = (x1 + x2) / 2.0f;
        final float vertMiddle  = (y1 + y2) / 2.0f;
        children = new Node[4];
        children[0] = new Node((int)Math.ceil(horizMiddle),
                               y1,
                               x2,
                               (int)Math.floor(vertMiddle));
        children[1] = new Node(x1,
                               y1,
                               (int)Math.floor(horizMiddle),
                               (int)Math.floor(vertMiddle));
        children[2] = new Node(x1,
                               (int)Math.ceil(vertMiddle),
                               (int)Math.floor(horizMiddle),
                               y2);
        children[3] = new Node((int)Math.ceil(horizMiddle),
                               (int)Math.ceil(vertMiddle),
                               x2,
                               y2);
    }

    /**
     * Checks if the node is empty (no point stored and no child nodes).
     *
     * @return true if node is empty, false otherwise
     */
    private boolean isEmpty()
    {
        return point == null && children == null;
    }

    /**
     * Counts the node's empty child nodes.
     *
     * @return number of empty child nodes or -1 if there are no children
     */
    private int countEmptyChildren()
    {
        if (children == null) {
            return -1;
        } else {
            int num = 0;
            for (int i = 0; i < children.length; ++i) {
                if (children[i].isEmpty()) {
                    ++num;
                }
            }
            return num;
        }
    }

    /**
     * Removes unnecessary nodes.
     */
    private void pruneLeaves()
    {
        if (children == null) {
            return;
        }

        for (int i = 0; i < children.length; ++i) {
            Node child = children[i];
            if (child.children == null) {
                continue;
            }
            if (child.countEmptyChildren() >= 3) {
                for (int j = 0; j < child.children.length; ++j) {
                    if (child.children[j].point != null && child.children[j].children == null) {
                        child.point = child.children[j].point;
                        child.children = null;
                        return;
                    }
                }
            }
        }
    }

    /* Area covered by this node. */
    int x1; // upper left corner
    int y1;
    int x2; // lower right corner
    int y2;
    /** The point stored in this node. */
    private Point point;
    /** Children of this node. */
    private Node[] children;
}

/**
 * A data structure used for storing points of an area.
 */
public final class QuadTree
{
    /**
     * Constructor.
     *
     * @param size width of the area
     */
    public QuadTree(int size)
    {
        this.size = size;
    }
    
    /**
     * Inserts a new point in the tree.
     *
     * @param point point to insert
     */
    public void Insert(Point point)
    {
        if (root == null) {
            root = new Node(0, 0, size-1, size-1);
        }
        root.insert(point);
        System.out.println();
        root.printPoints(System.out);
    }

    /**
     * Removes a point from the tree.
     * 
     * @param point point to remove
     */
    public void Remove(Point point)
    {
        if (root == null) {
            System.out.println("tree is empty");
        } else {
            boolean success = root.remove(point);
            if (!success) {
                System.out.println("point not found");
            }
            System.out.println();
            root.printPoints(System.out);
        }
    }

    /**
     * Checks if the tree contains the given point.
     *
     * @param point point to search for
     * @return true if point found in the tree
     */
    public boolean Find(Point point)
    {
        boolean found = false;
        if (root == null) {
            System.out.println("tree is empty");
        } else {
            if (root.find(point)) {
                found = true;
                System.out.println("found the point (" + point.x + ", " + point.y + ")");
            } else {
                System.out.println("didn't find the point (" + point.x + ", " + point.y + ")");
            }
        }
        return found;
    }

    /** Width of the area. */
    private final int size;
    /** Root of the tree. */
    private Node root;
}
